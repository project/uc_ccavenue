<?php
/**
 * @file
 * Handles checksum.
 */

/**
 * Custom function for getting checksum.
 * @param $merchant_id, $amount, $order_id, $url, $working_key.
 * @returns $adler.
 */
function uc_ccavenue_get_checksum($merchant_id, $amount, $order_id, $url, $working_key) {
  $string_value = "$merchant_id|$order_id|$amount|$url|$working_key";
  $adler = 1;
  $adler = uc_ccavenue_adler32($adler,$string_value);
  return $adler;
}

/**
 * Custom function to verify saved checksum.
 * @param $merchant_id, $order_id, $amount, $auth_desc, $checksum, $working_key
 * @returns $adler
 */
function uc_ccavenue_verify_checksum($merchant_id, $order_id, $amount, $auth_desc, $checksum, $working_key) {
  $string_value = "$merchant_id|$order_id|$amount|$auth_desc|$working_key";
  $adler = 1;
  // Getting checksum vlue of current string.
  $adler = uc_ccavenue_adler32($adler,$string_value);
  // Checksum comparing.
  if($adler == $checksum)
    return TRUE;
  else
    return FALSE;
}

/**
 * Custom function create checksum.
 * @param $adler,$str.
 * @returns $sum.
 */
function uc_ccavenue_adler32($adler, $string_value) {
  $base_value =  65521 ;
  $string_value1 = $adler & 0xffff ;
  $string_value2 = ($adler >> 16) & 0xffff;
  for($inc = 0; $inc < strlen($string_value); $inc++) {
    // Calculating with ascii value of the string.
    $string_value1 = ($string_value1 + Ord($string_value[$inc])) % $base_value ;
    $string_value2 = ($string_value2 + $string_value1) % $base_value ;
  }
  $sum = uc_ccavenue_left_shift($string_value2 , 16) + $string_value1;
  return $sum;
}

/**
 * Sub function to shift left for uc_ccavenue_adler32().
 * @param $computed_value, $shift_num.
 * @returns $shift_value.
 */
function uc_ccavenue_left_shift($computed_value, $shift_num) {
  $computed_value = DecBin($computed_value);
  for( $inc = 0; $inc < (64 - strlen($computed_value)); $inc++)
    $computed_value = "0" . $computed_value;

  for($inc = 0; $inc < $shift_num; $inc++) {
    $computed_value = $computed_value . "0";
    $computed_value = substr($computed_value, 1 );
  }
  // Get the decimal number of the computed value.
  $shifted_value = uc_ccavenue_cdec($computed_value);
  return $shifted_value;
}

/**
 * Sub function for uc_ccavenue_left_shift() to get the decimal number of
 *  the computed value.
 * @param $computed_value.
 * @returns $decimal_value.
 */
function uc_ccavenue_cdec($computed_value) {
  $decimal_value = 0;
  for ($inc = 0; $inc < strlen($computed_value); $inc++) {
    $temp = $computed_value[$inc];
    $decimal_value =  $decimal_value + $temp*pow(2, strlen($computed_value) - $inc - 1);
  }
  return $decimal_value;
}