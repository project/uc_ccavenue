<?php
/**
 * @file
 * CCAvenue administration menu items.
 */

/**
 * Page callback for CCAvenue redirect.
 */
function uc_ccavenue_redirect($order_id = 0) {
  watchdog('uc_ccavenue',
    'Receiving CCAvenue at URL for order @order_id. <pre>@debug</pre>',
    array('@order_id' => $order_id,
      '@debug' => variable_get('uc_ccavenue_debug', FALSE) ? print_r($_POST, TRUE) : ''
    )
  );

  $working_key = variable_get('uc_ccavenue_working_key', '');
  $merchant_id = $_REQUEST['Merchant_Id'];
  $amount = $_REQUEST['Amount'];
  $order_id = $_REQUEST['Order_Id'];
  $checksum = $_REQUEST['Checksum'];
  $auth_desc = $_REQUEST['AuthDesc'];

  $checksum_validate = uc_ccavenue_verify_checksum($merchant_id, $order_id, $amount, $auth_desc, $checksum, $working_key);

  // Validate if the order was correctly done.
  $order = uc_order_load($order_id, FALSE);

  if ($order == FALSE) {
    watchdog('uc_ccavenue', 'Return attempted for non-existent order.', array(), WATCHDOG_ERROR);
    return;
  }

  // Validate checksum.
  if ($checksum_validate != TRUE) {
    watchdog('uc_ccavenue', 'Checksum validation error for @order_id', array('@order_id' => $order_id), WATCHDOG_ERROR);
    return;
  }

  // Validate duplicate entries.
  if ($checksum_validate == TRUE) {
    $duplicate = db_query(
      "SELECT COUNT(*) FROM {uc_ccavenue} WHERE checksum = :checksum AND status <> :status",
      array(
        ':checksum' => $checksum,
        ':status' => 'Pending'
      )
    )
    ->fetchField();
    
    if ($duplicate > 0) {
      watchdog('uc_ccavenue', 'Transaction has been processed before.', array(), WATCHDOG_NOTICE);
      return;
    }
  }

  $payment_status = "";
  if ($auth_desc == "Y" || $auth_desc == "B") {
    $payment_status = "Completed";
  }
  elseif ($auth_desc == "N") {
    $payment_status = "Declined";
  }

  $receiver_email = uc_store_email_from();
  // Logging transaction details.
  uc_ccavenue_transaction_save($order_id, $merchant_id, $checksum, $amount, $payment_status, $receiver_email, $order->primary_email, $auth_desc);

  if ($payment_status == 'Completed') {
    db_update('uc_orders')
    ->fields(array(
      'order_status' => 'payment_received',
    ))
    ->condition('order_id', $order_id, '=')
    ->execute();
  }

  if ($checksum_validate == TRUE && $auth_desc == "Y") {
    // Here you need to put in the routines for a successful
    // transaction such as sending an email to customer,
    // setting database status, informing logistics etc.
    $comment = t('CCAvenue transaction checksum: @checksum', array('@checksum' => $checksum));
    uc_payment_enter($order_id, 'ccavenue', $amount, $order->uid, NULL, $comment);
    uc_cart_complete_sale($order);
    uc_order_comment_save($order_id, 0, t('Payment of @amount @currency submitted through CCAvenue.', array('@amount' => $amount, '@currency' => 'INR')), 'order', 'payment_received');
    uc_ccavenue_complete($order_id);
  }
  elseif ($checksum_validate == TRUE && $auth_desc == "B") {
    // Here you need to put in the routines/e-mail for a 
    // "Batch Processing" order.
    // This is only if payment for this transaction has been made by an
    // American Express Card since American Express authorisation
    // status is available only after 5-6 hours by mail from ccavenue
    // and at the "View Pending Orders".
    $comment = t('CCAvenue transaction checksum vis Amex CC: @checksum', array('@checksum' => $checksum));
    uc_payment_enter($order_id, 'ccavenue', $amount, $order->uid, NULL, $comment);
    uc_cart_complete_sale($order);
    uc_order_comment_save($order_id, 0, t('Payment of @amount @currency submitted through CCAvenue via Amex CC.', array('@amount' => $amount, '@currency' => 'INR')), 'order', 'payment_received');
    uc_ccavenue_complete($order_id);
  }
  elseif ($checksum_validate == TRUE && $auth_desc == "N") {
    // Here you need to put in the routines for a failed.
    // transaction such as sending an email to customer
    // setting database status etc.
    uc_order_comment_save($order_id, 0, t("The customer's attempted payment failed."), 'admin');
    uc_ccavenue_cancel();
  }
  else {
    // Here you need to simply ignore this and dont need
    // to perform any operation in this condition.
    uc_order_comment_save($order_id, 0, t('Security Error. Illegal access detected'), 'admin');
    uc_ccavenue_cancel();
  }
}

/**
 * Page callback to handle a complete CCAvenue Payment sale.
 * @param $order_id
 * 
 */
function uc_ccavenue_complete($order_id = 0) {
  watchdog('Order Complete', print_r($order_id, TRUE));

  if (intval($_SESSION['cart_order']) != $order_id) {
    $_SESSION['cart_order'] = $order_id;
  }

  if (FALSE === uc_order_load($order_id, FALSE)) {
    drupal_goto('cart');
  }
  // This lets us know it's a legitimate access of the complete page.
  $_SESSION['do_complete'] = TRUE;
  drupal_goto('cart/checkout/complete');
}

/**
 * Page callback to handle a canceled CCAvenue Payment sale.
 * 
 */
function uc_ccavenue_cancel() {
  unset($_SESSION['cart_order']);
  drupal_set_message(t('Your CCAvenue payment was cancelled. Please feel free to continue shopping or contact us for assistance.'));
  drupal_goto('cart');
}

/**
 * Custom function to log transaction details.
 * @param $order_id, $merchant_id, $checksum, $amount, $payment_status,
 *   $receiver_email, $order_email, $auth_desc.
 */
function uc_ccavenue_transaction_save($order_id, $merchant_id, $checksum, $amount, $payment_status, $receiver_email, $order_email, $auth_desc) {
  db_insert('uc_ccavenue')
  ->fields(array(
    'order_id' => $order_id,
    'merchant_id' => $merchant_id,
    'checksum' => $checksum,
    'amount' => $amount,
    'status' => $payment_status,
    'receiver_email' => $receiver_email,
    'payer_email' => $order_email,
    'authdesc' => $auth_desc,
    'received' => REQUEST_TIME,
  ))
  ->execute();
}